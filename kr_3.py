from random import uniform
from math import sqrt


def Monte_Karlo(n_min, n_max, step):
    arr_point, arr_pi = [], []

    inside = 0
    real_pi = 3.141592

    for i in range(n_min, n_max, step):
        for _ in range(step):
            if sqrt(uniform(0, 1) ** 2 + uniform(0, 1) ** 2) <= 1:
                inside += 1

        arr_point.append(i)
        arr_pi.append(4 * inside / i)

    arr_real_pi = [real_pi for _ in range(len(arr_point))]
    return arr_point, arr_pi, arr_real_pi
